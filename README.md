# hjm-reset.css

1. 尽量抹平不同元素在不同浏览器之间的差异。
1. 去掉元素自身的默认样式以保证在最大的可配置性。

## 使用方法

### 下载npm包
1. npm i hjm-reset.css

### 项目中引入
1. import 'hjm-reset.css'