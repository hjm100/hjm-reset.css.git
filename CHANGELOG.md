# 更新日志

## 1.0.0

1. 增加对img默认的底部3px问题进行处理
1. 增加input,button对父类元素字体的继承
1. 修正在所有浏览器下的line-height;
1. 去除input下appearance：none的属性。
1. 将body中长按选择改为除input/textarea外的长按选择(不兼容html编辑模式)